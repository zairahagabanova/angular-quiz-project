import { Component, Input } from '@angular/core';
import { Router } from "@angular/router";
import { QuizService } from '../shared/services/quiz.service';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent {
  @Input() categories: any[] = [];
  @Input() filteredCategories: any[] = [];
  inputCategory: string = "";
  

  constructor(private router: Router, private http: HttpClient) { }

  filterCategory() {
    this.filteredCategories = this.categories.filter(categorie => categorie?.categoryLabel.toLowerCase().includes(this.inputCategory.toLowerCase()))
    console.log(this.filteredCategories);
  }

  resetCategories() {
    this.filteredCategories = this.categories;
  }

  getCategories(){
    console.log("Categorie get");
  }

  ngOnInit(): void{
    this.http.get('http://localhost:3000/categories').subscribe((categoriesResult: any) => {
      this.categories = categoriesResult;
      this.filteredCategories = categoriesResult;
      console.log(this.categories);
      console.log(categoriesResult);
    });
  }

  navigateToQuiz() {
    this.router.navigate(['/categories']);
  }

  goToCategoryPage(categoryName: string){
    this.router.navigate(['/quiz', categoryName]);
  }
}
